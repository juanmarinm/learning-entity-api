<?php
function audio_entity_form($form, &$form_state, $entity, $operation, $entity_type) {

  $form['wrapper'] = array(
    '#title' => t('General'),
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
  );
  $form['wrapper']['name'] = array(
    '#title' => t('Name'),
    '#type' => 'textfield',
    '#default_value' => isset($entity->name) ? $entity->name : '',
    '#description' => t('Audio name'),
    '#required' => TRUE,
    '#weight' => -50,
  );

  $form['wrapper']['embed_code'] = array(
    '#title' => t('Embed Code'),
    '#type' => 'textfield',
    '#default_value' => isset($entity->embed_code) ? $entity->embed_code : '',
    '#description' => t('Audio embedcode'),
    '#required' => TRUE,
  );

  $form['wrapper']['duration'] = array(
    '#title' => t('Duration'),
    '#type' => 'textfield',
    '#default_value' => isset($entity->duration) ? $entity->duration : '',
    '#description' => t('Duration of the audio in seconds'),
    '#required' => TRUE,
  );

  field_attach_form($entity_type, $entity, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => isset($entity->is_new) ? t('Save audio') : t('Update audio'),
    ),
  );

  return $form;
}

/**
 * Validation for audio entity form.
 */
function audio_entity_form_validate($form, &$form_state) {
  if (isset($form_state['values']['duration']) && !is_numeric($form_state['values']['duration'])) {
    form_set_error('duration', t('Duration field must be an integer value.'));
  }
}

/**
 * Submit handler for audio entity form.
 */
function audio_entity_form_submit($form, &$form_state) {

  //entity_ui_form_submit_build_entity. See entity.ui.inc file for more information.
  $entity = entity_ui_form_submit_build_entity($form, $form_state);
  $entity->save();
  drupal_set_message(t('@name audio has been saved.', array('@name' => $entity->name)));
  $form_state['redirect'] = 'admin/audio_entity';
}

